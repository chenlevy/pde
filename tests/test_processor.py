import pytest
from pde import processor as mut


def test_process_line():
    ans = mut.process_line(
        "file.name:1234:...U:uP:p...-----K-----/key/-----K-----..."
    )
    assert next(ans) == "file.name:1234:user:u password:p\n"
    assert next(ans) == "file.name:1234:Key: /key/\n"
    # and no more
    with pytest.raises(StopIteration):
        next(ans)


def test_process():
    lines = """\
a.file:1:no match here
a.file:2:... user:some@user pass:somePASS ...
b.file:1:no match
b.file:2:
b.file:3:... -----K-----/key/-----K----- ...
b.file:4:...U:uP:p...-----K-----/key/-----K-----...
c.file:1:
""".splitlines()
    ans = mut.process(lines)
    assert next(ans) == "a.file:2:user:some@user password:somePASS\n"
    assert next(ans) == "b.file:3:Key: /key/\n"
    assert next(ans) == "b.file:4:user:u password:p\n"
    assert next(ans) == "b.file:4:Key: /key/\n"
    # and no more
    with pytest.raises(StopIteration):
        next(ans)


def test_process_bin():
    lines = b"""\
a.file:1:no match here
a.file:2:... user:some@user pass:somePASS ...
b.file:1:no match
b.file:2:
b.file:3:... -----K-----/key/-----K----- ...
b.file:4:...U:uP:p...-----K-----/key/-----K-----...
c.file:1:
""".splitlines()
    ans = mut.process_bin(lines)
    assert next(ans) == b"a.file:2:user:some@user password:somePASS\n"
    assert next(ans) == b"b.file:3:Key: /key/\n"
    assert next(ans) == b"b.file:4:user:u password:p\n"
    assert next(ans) == b"b.file:4:Key: /key/\n"
    # and no more
    with pytest.raises(StopIteration):
        next(ans)
