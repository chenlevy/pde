import pytest
from pde import parse as mut


@pytest.mark.parametrize("filename", ["a", "normal.file", "a" * 25])
def test_validate_filename_pass(filename):
    mut.validate_filename(filename)  # no exception


@pytest.mark.parametrize(
    "filename", ["", "a" * 26, "a" * 100000],
)
def test_validate_filename_fail(filename):
    with pytest.raises(ValueError):
        mut.validate_filename(filename)


@pytest.mark.parametrize("line_number", ["0", "1234", "9" * 1000])
def test_validate_line_number_pass(line_number):
    mut.validate_line_number(line_number)  # no exception


@pytest.mark.parametrize(
    "line_number", ["a", "3.14", "-1"],
)
def test_validate_line_number_fail(line_number):
    with pytest.raises(ValueError):
        mut.validate_line_number(line_number)


@pytest.mark.parametrize(
    "extracted_string", ["", "a", "foo bar baz", "x" * 8192]
)
def test_validate_extracted_string_pass(extracted_string):
    mut.validate_extracted_string(extracted_string)  # no exception


@pytest.mark.parametrize("extracted_string", ["x" * 8193, "x" * 100000,])
def test_validate_extracted_string_fail(extracted_string):
    with pytest.raises(ValueError):
        mut.validate_extracted_string(extracted_string)


def test_validate_parsed_line_pass():
    mut.validate_parsed_line(
        mut.ParsedLine("file.name", "123", "extracted string")
    )  # no exception


@pytest.mark.parametrize(
    "parsed_line",
    [
        # no file name
        mut.ParsedLine("", "1234", "extracted string"),
        # illegal line number
        mut.ParsedLine("file.name", "-3.14", "extracted_string"),
        # too long extracted string
        mut.ParsedLine("file.name", "1234", "x" * 8193),
    ],
)
def test_validate_parsed_line_fail(parsed_line):
    with pytest.raises(ValueError):
        mut.validate_parsed_line(parsed_line)


def test_parse_line():
    assert mut.parse_line("file.name:1234:extracted:string") == mut.ParsedLine(
        "file.name", "1234", "extracted:string"
    )


def test_parse_and_validate_line_pass():
    assert mut.parse_and_validate_line(
        "file.name:1234:extracted:string"
    ) == mut.ParsedLine("file.name", "1234", "extracted:string")


def test_parse_and_validate_line_fail():
    with pytest.raises(ValueError):
        mut.parse_and_validate_line("file.name:12.34:extracted:string")
