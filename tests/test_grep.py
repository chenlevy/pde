import pytest
from pde import grep as mut

(SHORT_USER, LONG_USER, KEY) = mut.DESERS


def assert_same_iters(ai, bi):
    # then iterators has the same items
    for (a, b) in zip(ai, bi):
        assert a == b

    # and both iterators are exhausted
    with pytest.raises(StopIteration):
        next(ai)
    with pytest.raises(StopIteration):
        next(bi)


def assert_needls_found(haystack, deser, needles):
    def do_test(haystack):
        # given
        actual = mut.needles(haystack, deser)
        expected = iter(needles)
        # then
        assert_same_iters(actual, expected)

    do_test(haystack)
    do_test("with prefix {}".format(haystack))
    do_test("{} with suffix".format(haystack))
    do_test("with prefix {} and suffix".format(haystack))


@pytest.mark.parametrize(
    "haystack, needles",
    [
        (
            "U:chen@rotemlevy.name P:verySECRETE",
            ["user:chen@rotemlevy.name password:verySECRETE"],
        ),
        (
            "U:chen@rotemlevy.name P:",  # empty password
            ["user:chen@rotemlevy.name password:"],
        ),
        (
            "U:u1..P:p1...U:u2..P:p2",  # multiple matches
            ["user:u1 password:p1", "user:u2 password:p2"],
        ),
    ],
)
def test_short_user_needles_found(haystack, needles):
    assert_needls_found(haystack, SHORT_USER, needles)


@pytest.mark.parametrize(
    "haystack",
    [
        ":chen@rotemlevy.name P:verySECRETE",  # no U:
        "U:chen@rotemlevy.name :verySECRETE",  # no P:
        "U:P:verySECRETE",  # empty user
        "U:&chen@rotemlevy.name P:verySECRETE",  # invalid user name:
    ],
)
def test_short_user_needle_not_found(haystack):
    with pytest.raises(StopIteration):
        next(mut.needles(haystack, SHORT_USER))


@pytest.mark.parametrize(
    "haystack, needles",
    [
        (
            "user:johnDow@example.com..pass:thisISmyPASSWORD",
            ["user:johnDow@example.com password:thisISmyPASSWORD"],
        ),
        (
            "user:johnDow@example.com..pass:",  # empty password
            ["user:johnDow@example.com password:"],
        ),
        (
            "user:u1..pass:p1...user:u2..pass:p2",  # multiple matches
            ["user:u1 password:p1", "user:u2 password:p2"],
        ),
    ],
)
def test_long_user_needles_found(haystack, needles):
    assert_needls_found(haystack, LONG_USER, needles)


@pytest.mark.parametrize(
    "haystack",
    [
        "uSer:johnDow@eample.compass:thisISmyPASSWORD",  # no user:
        "user:johnDow@eample.compas:thisISmyPASSWORD",  # no pass:
        "user:pass:thisISmyPASSWORD",  # empty username
        "user:%johnDow@eample.compass:thisISmyPASSWORD",  # no pass:
    ],
)
def test_long_user_needle_not_found(haystack):
    with pytest.raises(StopIteration):
        next(mut.needles(haystack, LONG_USER))


@pytest.mark.parametrize(
    "haystack, needles",
    [
        (
            "----- KEY BEGIN -----ABC+abc/123=----- KEY END -----",
            ["Key: ABC+abc/123="],
        ),
        (
            "-----A-----ka-----A-----...-----B-----kb-----B-----",
            ["Key: ka", "Key: kb"],
        ),
        (
            "-----A-----k1-----A----------B-----k2-----B-----",
            ["Key: k1", "Key: k2"],
        ),
    ],
)
def test_key_needle_found(haystack, needles):
    assert_needls_found(haystack, KEY, needles)


@pytest.mark.parametrize(
    "haystack",
    [
        # msiing `-` in header/footer
        "---- KEY BEGIN -----ABC+abc/123=----- KEY END -----",
        "----- KEY BEGIN ----ABC+abc/123=----- KEY END -----",
        "----- KEY BEGIN -----ABC+abc/123=---- KEY END -----",
        "----- KEY BEGIN -----ABC+abc/123=----- KEY END ----",
        # invalid character in base64 or header/footer has too many `-`
        "----- KEY BEGIN ------ABC+abc/123=----- KEY END -----",
        "----- KEY BEGIN -----ABC+abc/123=------ KEY END -----",
        "----- KEY BEGIN -----ABC+abc%123=----- KEY END -----",
        # padding too long
        "----- KEY BEGIN -----ABC+abc/123====----- KEY END -----",
    ],
)
def test_key_pattern_not_match(haystack):
    with pytest.raises(StopIteration):
        next(mut.needles(haystack, KEY))


@pytest.mark.parametrize(
    "haystack, needles",
    [
        ("no-match", []),
        ("prefix U:u P:p suffix", ["user:u password:p"]),
        ("prefix -----A-----Key/123-----B----- suffix", ["Key: Key/123"]),
        (
            "...user:u...pass:p...-----K-----/key/-----K-----...",
            ["user:u password:p", "Key: /key/"],
        ),
    ],
)
def test_needles_and_pins_found(haystack, needles):
    assert_same_iters(mut.needles_and_pins(haystack), iter(needles))
