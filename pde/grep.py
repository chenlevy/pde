from collections import namedtuple
from itertools import chain
from typing import Iterator, List, NewType
import re


# Text pattern for deconstruction of pattern to search and
# re-construction the finds in the format the user expects
DeCon = namedtuple("DeCon", "deconstruct, construct")  # patterns
DECONS: List[DeCon] = [
    DeCon(
        deconstruct=(
            r"U:("  # user
            r"[A-Za-z0-9]+"  # name
            r"(?:@[A-Za-z0-9]+(?:\.[A-Za-z0-9]+){0,2})?"  # @domain?
            r")"  # /user
            r".*?"  # anything non-greedy
            r"P:([A-Za-z0-9]*)"  # password
        ),
        construct="user:{} password:{}",
    ),
    DeCon(
        deconstruct=(
            r"user:("  # user
            r"[A-Za-z0-9]+"  # name
            r"(?:@[A-Za-z0-9]+(?:\.[A-Za-z0-9]+){0,2})?"  # @domain?
            r")"  # /user
            r".*?"  # anything (non greedy)
            r"pass:([A-Za-z0-9]*)"  # password
        ),
        construct="user:{} password:{}",
    ),
    DeCon(
        deconstruct=(
            r"-----[A-Z ]+-----"  # header
            r"("  # capture
            r"[+/0-9A-Za-z]+"  # base64
            r"={0,3}"  # padding
            r")"  # /capture
            r"-----[A-Z ]+-----"  # footer
        ),
        construct="Key: {}",
    ),
    # TODO: add more DeCon(...) patterns here
]


# Deserialization and serialization functions, based on the DECONS
# patterns
DeSer = namedtuple("DeSer", "deserialize, serialize")  # functions
DESERS = [
    DeSer(
        deserialize=re.compile(decon.deconstruct).findall,
        serialize=decon.construct.format,
    )
    for decon in DECONS
]

Haystack = NewType("Haystack", str)
Needle = NewType("Needle", str)


def needles(haystack: Haystack, deser: DeSer) -> Iterator[Needle]:
    """Find in `haystack` all needles specific of a type given by `deser`
    :param haystack: line of text to search in
    :param deser: functions that defined how to extracted the needles type.
    :return: iterator over a specific type of needle
    """
    return (
        deser.serialize(
            # NOTE: Match.findall() might return a list of tuples or
            #       strings, and iterating over strings yields one
            #       char at a time. This converts the all results into
            #       tuples, so they will be have uniform behaviour.
            *(item if isinstance(item, tuple) else (item,))
        )
        for item in deser.deserialize(haystack)
    )


def needles_and_pins(haystack: Haystack) -> Iterator[Needle]:
    """Find all types of needles in `haystack`
    Search haystack for all patterns.
    :param haystack: line of text to search in
    :return: iterator over all types of needle

    """
    return chain.from_iterable(needles(haystack, deser) for deser in DESERS)
