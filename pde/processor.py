from itertools import chain
from typing import Iterator, Sequence

from pde.grep import needles_and_pins
from pde.parse import parse_and_validate_line


def process_line(line: str, logger=None) -> Iterator[str]:
    """Get an input line, and process it all the way yielding all the
    matches that line may have in the required format.

    """
    try:
        parsed_line = parse_and_validate_line(line)
        return (
            "{filename}:{line_number}:{needle_or_pin}\n".format(
                **parsed_line._asdict(), needle_or_pin=needle_or_pin
            )
            for needle_or_pin in needles_and_pins(parsed_line.extracted_string)
        )
    except ValueError as e:
        if logger is not None:
            logger.warning(e)


def process(lines: Sequence[str], logger=None) -> Iterator[str]:
    """Process multiple input lines in sequence"""
    return chain.from_iterable(process_line(line, logger) for line in lines)


def process_bin(line: Sequence[bytes], logger=None) -> Iterator[bytes]:
    return (o.encode() for o in process(i.decode() for i in line))
