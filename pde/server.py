import logging
import socketserver
from pde.processor import process_bin

logging.basicConfig(filename="server.log", level=logging.WARNING)
logger = logging.getLogger("pde.server")


class PDEHandler(socketserver.StreamRequestHandler):
    """Handle Pattern Detection Engine Requests"""

    def handle(self):
        self.wfile.writelines(process_bin(self.rfile, logger))


if __name__ == "__main__":
    HOST, PORT = "0.0.0.0", 9999

    with socketserver.ForkingTCPServer((HOST, PORT), PDEHandler) as server:
        print(
            "Listening on {}:{} - press Ctrl-C to stop the server".format(
                HOST, PORT
            )
        )
        server.serve_forever()
