from collections import namedtuple
from typing import NewType


Filename = NewType("Filename", str)


def validate_filename(filename: Filename):
    if not 0 < len(filename) <= 25:
        raise ValueError("Filename must be between 1 and 25 characters long.")


# NOTE: We only validate this number and passing it along, so it can
#       stay a string.
LineNumber = NewType("LineNumber", str)


def validate_line_number(line_number: LineNumber):
    if int(line_number) < 0:
        raise ValueError("LineNumber must be a positive integer")


ExtractedString = NewType("ExtractedString", str)


def validate_extracted_string(extracted_string: ExtractedString):
    if len(extracted_string) > 8192:
        raise ValueError(
            "ExtractedString must not be longer than 8K characters"
        )


ParsedLine = namedtuple(
    "ParsedLine", "filename, line_number, extracted_string"
)


def validate_parsed_line(parse_line: ParsedLine):
    validate_filename(parse_line.filename)
    validate_line_number(parse_line.line_number)
    validate_extracted_string(parse_line.extracted_string)


######################################################################


def parse_line(line: str) -> ParsedLine:
    try:
        return ParsedLine(*line.split(":", 2))
    except TypeError:
        raise (ValueError(line))


def parse_and_validate_line(line: str) -> ParsedLine:
    parseed_line = parse_line(line)
    validate_parsed_line(parseed_line)
    return parseed_line
