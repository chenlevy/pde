all: run

clean:
	rm -rf venv && rm -rf *.egg-info && rm -rf dist && rm -rf *.log* && rm -fr pde/__pycache__ && rm -fr tests/__pycache__

venv:
	virtualenv --python=python3 venv && venv/bin/python setup.py develop

run: venv
	python pde/server.py

test: venv
	venv/bin/python -m pytest tests

sdist: venv test
	venv/bin/python setup.py sdist
