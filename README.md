[![pipeline status](https://gitlab.com/chenlevy/pde/badges/master/pipeline.svg)](https://gitlab.com/chenlevy/pde/-/commits/master)
[![coverage report](https://gitlab.com/chenlevy/pde/badges/master/coverage.svg)](https://gitlab.com/chenlevy/pde/-/commits/master)

# Pattern Detection Engine

- Author: Chen Rotem Levy <chen@rotemlevy.name>
- License: [Python Software Foundation License](https://docs.python.org/3/license.html#terms-and-conditions-for-accessing-or-otherwise-using-python)


## description

Detect patterns such as: `U:foo P:bar, user:chen pass:xxx and  -----K-----/key/-----K-----` in a file.


## Quick Start

Run the server:

    make run

Than you can use a command such as `nc` to send data to the server e.g.

    grep -Hn . | nc <host> 9999


## Prerequisites

To run the app you only need CPython 3.6+.
To run the test [PyTest](https://docs.pytest.org/) is also required.


## Development environment and release process

 - create virtualenv with: `make venv`

 - run tests: `make test`

 - create source distribution: `make sdist` (will run tests first)

 - to remove virtualenv and built distributions: `make clean`

 - to add more python dependencies: add to `install_requires` in `setup.py`

 - To add patterns look for TODO comment in `pde/grep.py`
